import os

import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize("name", [
    'vim-enhanced', 'git', 'zsh', 'unzip'
])
def test_packages(host, name):
    assert host.package(name).is_installed


@pytest.mark.parametrize("path", [
    "/opt/.oh-my-zsh",
    "/opt/pure",
    "/etc/skel/.zshrc"
])
def test_tools(host, path):
    assert host.file(path).exists
